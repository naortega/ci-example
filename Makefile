CC=gcc
OBJ=src/main.o
CFLAGS=-O2

%.o:%.c
	$(CC) -c -o $@ $< $(CFLAGS)

ci-example: $(OBJ)
	$(CC) -o $@ $^ $(CFLAGS)

.PHONY: clean

clean:
	rm -f $(OBJ)
